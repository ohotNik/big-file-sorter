package ru.ohotnik.laboratory.filesorter.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;

import static java.lang.Integer.parseInt;

/**
 * @author aleksander okhonchenko <strunasa@mail.ru>
 * @since 08.07.2018.
 */
public class AppGenerator {

  private static final SecureRandom RANDOM = new SecureRandom();


  public static void main(String[] args) throws IOException {
    if (args.length != 2) {
      throw new IllegalArgumentException("Use args: lines max_line_length");
    }
    int lines = parseInt(args[0]);
    int maxLineSize = parseInt(args[1]);
    assert lines > 0;
    assert maxLineSize > 0;

    final int minSize = maxLineSize == 1 ? 1 : maxLineSize / 2;
    final int randomSize = maxLineSize - minSize + 1;

    File file = new File(System.currentTimeMillis() + ".txt");
    //noinspection ResultOfMethodCallIgnored
    file.createNewFile();
    try (final FileWriter writer = new FileWriter(file)) {
      for (int i = 0; i < lines; i++) {
        writer.append(getString(minSize + RANDOM.nextInt(randomSize)));
      }
    }

  }

  private static String getString(final int size) {
    int aSymbol = 97;
    int zSymbol = 122;
    StringBuilder buffer = new StringBuilder(size);
    for (int i = 0; i < size; i++) {
      int randomLimitedInt = aSymbol + (int)
          (RANDOM.nextFloat() * (zSymbol - aSymbol + 1));
      buffer.append((char) randomLimitedInt);
    }
    return buffer.append('\n').toString();
  }

}
