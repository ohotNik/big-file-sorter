package ru.ohotnik.laboratory.filesorter.sorter;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

/**
 * @author aleksander okhonchenko <strunasa@mail.ru>
 * @since 08.07.2018.
 */
public class App {

  public static void main(String[] args) throws IOException {
    final long freeMemory = Runtime.getRuntime().freeMemory();
    final long maxSize = (long) (0.25 * freeMemory);
    final String targetFileName = args[0];
    final File file = new File(targetFileName);
    assert file.exists();
    final List<File> tempFiles = new ArrayList<>();

    //разбрасываем файл по временным файлам меньшего размера. Сразу сортируем.
    //размер первого файла определяем исходя из свободной памяти
    try (FileReader fR = new FileReader(file);
         BufferedReader bufferedReader = new BufferedReader(fR)) {
      String line;
      File tempFile = File.createTempFile(targetFileName, tempFiles.size() + "");
      tempFiles.add(tempFile);
      FileWriter writer = new FileWriter(tempFile);
      List<String> lines = new ArrayList<>();
      int size = 0;
      int cnt = 0;

      while ((line = bufferedReader.readLine()) != null) {
        cnt++;
        if ((size != 0 && cnt > size) || (size == 0 && Runtime.getRuntime().freeMemory() < maxSize)) {
          Collections.sort(lines);
          for (String l : lines) {
            writer.append(l).append('\n');
          }
          writer.flush();
          writer.close();
          lines.clear();
          ((ArrayList<String>) lines).trimToSize();
          tempFile = File.createTempFile(targetFileName, tempFiles.size() + "");
          writer = new FileWriter(tempFile);
          tempFiles.add(tempFile);
          size = size == 0 ? cnt : size;
          cnt = 0;
        }
        lines.add(line);
      }

      //последний файл
      Collections.sort(lines);
      for (String l : lines) {
        writer.append(l).append('\n');
      }
      writer.flush();
      writer.close();
    }

    //будем читать сразу отовсюду
    PriorityQueue<ReadOption> options = new PriorityQueue<>();
    for (File f : tempFiles) {
      FileReader fR = new FileReader(f);
      BufferedReader bufferedReader = new BufferedReader(fR);
      final ReadOption option = new ReadOption(bufferedReader.readLine(), bufferedReader);
      if (option.line != null) {
        options.add(option);
      }
    }

    File sortedResult = new File(file.getName() + "_sorted");
    sortedResult.createNewFile();
    try (FileWriter writer = new FileWriter(sortedResult)) {
      ReadOption option;
      String line;
      while (!options.isEmpty()) {
        option = options.poll();
        writer.append(option.line).append('\n');
        if ((line = option.bufferedReader.readLine()) != null) {
          options.add(new ReadOption(line, option.bufferedReader));
        }
      }
    }

    for (File f : tempFiles) {
      f.delete();
    }
    file.delete();
    sortedResult.renameTo(file.getAbsoluteFile());

    System.out.println();
  }

  private static class ReadOption implements Comparable<ReadOption> {
    String line;
    BufferedReader bufferedReader;

    ReadOption(String line, BufferedReader bufferedReader) {
      this.line = line;
      this.bufferedReader = bufferedReader;
    }

    @Override
    public int compareTo(ReadOption o) {
      return line.compareTo(o.line);
    }
  }

}
